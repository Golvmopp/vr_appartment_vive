﻿using UnityEngine;
using System.Collections;

public class Draw : MonoBehaviour {

    public VRInputLeft m_VRInput;
    public AudioClip spraySound;

    private Vector3 firstDrawPoint;
    private Vector3 secondDrawPoint;

    private bool newPoint = true;

    private Coroutine m_DrawRoutine;
    private Coroutine m_PlaySoundRoutine;

    //to draw
    private LineRenderer lineRenderer;

    private GameObject temp;
    private AudioSource source;

    private ArrayList positionsArray = new ArrayList();
    private ArrayList drawingsArray = new ArrayList();

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start()
    {
        //TODO: Ta reda på varför nedanstående kommentar inte fungerar
        //m_VRInput = GameObject.FindWithTag("LeftController").GetComponent<VRInputLeft>();
    }
    private void OnEnable()
    {
        m_VRInput.OnGripPressDown += GripPressDown;
        m_VRInput.OnGripPressUp += GripPressUp;
        m_VRInput.OnTouchPadPressDown += RemoveDrawings;
    }

    private void OnDisable()
    {
        m_VRInput.OnGripPressDown -= GripPressDown;
        m_VRInput.OnTriggerPressDown -= GripPressUp;
        m_VRInput.OnTouchPadPressDown -= RemoveDrawings;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void GripPressDown()
    {
        m_DrawRoutine = StartCoroutine(DrawLine());
        m_PlaySoundRoutine = StartCoroutine(PlaySound());
    }

    private void GripPressUp()
    {
        newPoint = true;
        if(m_DrawRoutine != null)
        {
            StopCoroutine(m_DrawRoutine);
            drawingsArray.Add(temp);
        }
        if(m_PlaySoundRoutine != null)
        {
            StopCoroutine(m_PlaySoundRoutine);
            source.Stop();
        }
    }

    private IEnumerator DrawLine()
    {
        temp = new GameObject();
        lineRenderer = temp.AddComponent<LineRenderer>();
        lineRenderer.SetColors(Color.blue, Color.blue);
        lineRenderer.SetWidth(0.01f, 0.01f);

        while (true)
        {
            if (!newPoint)
            {
                secondDrawPoint = m_VRInput.transform.position;

                positionsArray.Add(secondDrawPoint);
                lineRenderer.SetVertexCount(positionsArray.Count);

                for (int i = 0; i < positionsArray.Count; i++)
                {
                    lineRenderer.SetPosition(i, (Vector3)positionsArray[i]);
                }
            }
            else if (newPoint)
            {
                positionsArray = new ArrayList();

                firstDrawPoint = m_VRInput.transform.position;
                positionsArray.Add(firstDrawPoint);
                newPoint = false;
            }

            yield return null;
        }
    }

    private IEnumerator PlaySound()
    {
        while(true)
        {
            source.PlayOneShot(spraySound, 1f);
            yield return new WaitForSeconds(2f);
        }
    }

    public void RemoveDrawings()
    {
        for(int i = 0; i < drawingsArray.Count; i++)
        {
            Destroy((GameObject)drawingsArray[i]);
        }
    }
}
