﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LeftControllerCanvasBehavior : MonoBehaviour {

    private VRInputLeft m_VRInput;
    private Text measurementText;
    // Use this for initialization
    void Start () {
        m_VRInput = GameObject.FindWithTag("LeftController").GetComponent<VRInputLeft>();
        measurementText = transform.GetChild(0).GetChild(0).GetComponent<Text>();

        this.transform.SetParent(m_VRInput.transform);
    }

    // Update is called once per frame
    void Update () {
	
	}

    public Text GetText()
    {
        return measurementText;
    }

    public void SetText(string text)
    {
        measurementText.text = text;
    }
}
