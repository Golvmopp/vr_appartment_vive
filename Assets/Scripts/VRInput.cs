﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class VRInput : MonoBehaviour {

    public event Action OnTriggerPress;
    public event Action OnTriggerPressDown;
    public event Action OnTriggerPressUp;

    public event Action<InteractableItem> OnGripPress;
    public event Action<InteractableItem> OnGripPressDown;
    public event Action OnGripPressUp;

    public event Action<InteractableItem> OnTouchPadPress;
    public event Action<InteractableItem> OnTouchPadPressDown;
    public event Action OnTouchPadPressUp;

    private Valve.VR.EVRButtonId triggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
    private Valve.VR.EVRButtonId gripButton = Valve.VR.EVRButtonId.k_EButton_Grip;
    private Valve.VR.EVRButtonId touchPad = Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;

    private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
    private SteamVR_TrackedObject trackedObj;

    HashSet<InteractableItem> objectsHoveringOver = new HashSet<InteractableItem>();

    private InteractableItem closestItem;
    private InteractableItem interactingItem;

    // Use this for initialization
    void Start () {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    // Update is called once per frame
    void Update () {
        if (controller == null)
        {
            Debug.Log("Controller is not initialized");
            return;
        }

        CheckInput();
	}

    private void CheckInput()
    {
        if (controller.GetPress(triggerButton)){
            if(OnTriggerPress != null)
            {
                OnTriggerPress();
            }
        }
        if (controller.GetPressDown(triggerButton)){
            if(OnTriggerPressDown != null)
            {
                OnTriggerPressDown();
            }
        }
        if (controller.GetPressUp(triggerButton))
        {
            if(OnTriggerPressUp != null)
            {
                OnTriggerPressUp();
            }
        }

        if (controller.GetPress(gripButton)){

        }
        if (controller.GetPressDown(gripButton)){
            float minDistance = float.MaxValue;

            float distance;
            foreach (InteractableItem item in objectsHoveringOver)
            {
                distance = (item.transform.position - transform.position).sqrMagnitude;

                if (distance < minDistance)
                {
                    minDistance = distance;
                    closestItem = item;
                }
            }

            interactingItem = closestItem;
            closestItem = null;

            if (OnGripPressDown != null)
            {
                OnGripPressDown(interactingItem);
            }
        }
        if (controller.GetPressUp(gripButton)){
            if(OnGripPressUp != null)
            {
                OnGripPressUp();
            }
        }

        if (controller.GetPress(touchPad))
        {
            float minDistance = float.MaxValue;

            float distance;
            foreach (InteractableItem item in objectsHoveringOver)
            {
                distance = (item.transform.position - transform.position).sqrMagnitude;

                if (distance < minDistance)
                {
                    minDistance = distance;
                    closestItem = item;
                }
            }

            interactingItem = closestItem;
            closestItem = null;
            if (OnTouchPadPress != null)
            {
                OnTouchPadPress(interactingItem);
            }
        }
        if (controller.GetPressDown(touchPad))
        {
            float minDistance = float.MaxValue;

            float distance;
            foreach (InteractableItem item in objectsHoveringOver)
            {
                distance = (item.transform.position - transform.position).sqrMagnitude;

                if (distance < minDistance)
                {
                    minDistance = distance;
                    closestItem = item;
                }
            }

            interactingItem = closestItem;
            closestItem = null;
            if (OnTouchPadPressDown != null)
            {
                OnTouchPadPressDown(interactingItem);
            }
        }
        if (controller.GetPressUp(touchPad))
        {
            if (OnTouchPadPressUp != null)
            {
                OnTouchPadPressUp();
            }
        }

    }

    private void OnTriggerEnter(Collider collider)
    {
        InteractableItem collidedItem = collider.GetComponent<InteractableItem>();
        if (collidedItem)
        {
            objectsHoveringOver.Add(collidedItem);
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        InteractableItem collidedItem = collider.GetComponent<InteractableItem>();
        if (collidedItem)
        {
            objectsHoveringOver.Remove(collidedItem);
        }
    }

    private void OnDestroy()
    {
        OnTriggerPress = null;
        OnTriggerPressDown = null;
        OnTriggerPressUp = null;

        OnGripPress = null;
        OnGripPressDown = null;
        OnGripPressUp = null;

        OnTouchPadPress = null;
        OnTouchPadPressDown = null;
        OnTouchPadPressUp = null;
    }
}
