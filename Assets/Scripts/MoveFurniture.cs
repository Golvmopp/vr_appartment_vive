﻿using UnityEngine;
using System.Collections;

public class MoveFurniture : MonoBehaviour {
    public VRInput m_VRInput;
    public AudioClip pickUpSound;
    public InteractableItem m_InteractableItem;

    private Rigidbody rigidbody;
    private AudioSource source;

    void Awake()
    {
        source = GameObject.Find("Audio").GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update () {
	
	}

    private void OnEnable()
    {
        m_VRInput.OnGripPressDown += GripPressDown;
        m_VRInput.OnGripPressUp += GripPressUp;
    }

    private void OnDisable()
    {
        m_VRInput.OnGripPressDown -= GripPressDown;
        m_VRInput.OnGripPressUp -= GripPressUp;
    }

    public void GripPressDown(InteractableItem interactableItem)
    {
        if (interactableItem == m_InteractableItem)
        {
            this.transform.parent = m_VRInput.transform;
            rigidbody.isKinematic = true;

            //Play sound
            source.PlayOneShot(pickUpSound, 1f);
        }
    }

    public void GripPressUp()
    {
        this.transform.parent = null;
        rigidbody.isKinematic = false;
    }
}
