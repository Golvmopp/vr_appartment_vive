﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Measurement : MonoBehaviour {

    public VRInputLeft m_VRInput;

    private int measurementPositions;

    private Vector3 firstMeasurement;
    private Vector3 secondMeasurement;
    private float distance;

    private GameObject leftControllerCanvas;

    private LineRenderer lineRenderer;

    private string measurementText;

    private GameObject canvas;
    private GameObject temp;

    private ArrayList markersArray = new ArrayList();

    // Use this for initialization
    void Start () {
        //TODO: Ta reda på varför nedanstående kommentar inte fungerar
        //m_VRInput = GameObject.FindWithTag("LeftController").GetComponent<VRInputLeft>();

        measurementPositions = 0;

        AddCanvasToController();
    }
    private void OnEnable()
    {
        m_VRInput.OnTriggerPress += TriggerPress;
        m_VRInput.OnTriggerPressDown += TriggerPressDown;
    }

    private void OnDisable()
    {
        m_VRInput.OnTriggerPress -= TriggerPress;
        m_VRInput.OnTriggerPressDown -= TriggerPressDown;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void TriggerPress()
    {

    }

    private void TriggerPressDown()
    {
        GameObject measurementMarker = new GameObject();
        GameObject measurementMarkerInstance = new GameObject();

        if (measurementPositions == 0)
        {
            firstMeasurement = m_VRInput.transform.position;

            measurementMarker = (GameObject)Resources.Load("MeasurementMarker");
            measurementMarkerInstance = (GameObject) Instantiate(measurementMarker, m_VRInput.transform.position, Quaternion.identity);
            markersArray.Add(measurementMarkerInstance);

            measurementText = "First position: " + firstMeasurement;
            canvas.GetComponent<LeftControllerCanvasBehavior>().SetText(measurementText);

            measurementPositions = 1;
        }
        else if(measurementPositions == 1)
        {
            temp = new GameObject();
            lineRenderer = temp.AddComponent<LineRenderer>();
            lineRenderer.SetColors(Color.cyan, Color.cyan);
            lineRenderer.SetWidth(0.01f, 0.01f);

            secondMeasurement = m_VRInput.transform.position;

            measurementMarker = (GameObject)Resources.Load("MeasurementMarker");
            measurementMarkerInstance = (GameObject) Instantiate(measurementMarker, m_VRInput.transform.position, Quaternion.identity);
            markersArray.Add(measurementMarkerInstance);
            markersArray.Add(temp);

            lineRenderer.SetPosition(0, firstMeasurement);
            lineRenderer.SetPosition(1, secondMeasurement);

            distance = Vector3.Distance(firstMeasurement, secondMeasurement);

            measurementText = "Distance: " + distance;
            canvas.GetComponent<LeftControllerCanvasBehavior>().SetText(measurementText);

            measurementPositions = 2;
        }
        else if(measurementPositions == 2)
        {
            RemoveMarkers();
            measurementPositions = 0;
        }
    }

    private void AddCanvasToController()
    {
        leftControllerCanvas = (GameObject)Resources.Load("LeftControllerCanvas");

        Vector3 newPos = new Vector3(m_VRInput.transform.position.x, m_VRInput.transform.position.y + 0.1f, m_VRInput.transform.position.z);

        canvas = (GameObject) Instantiate(leftControllerCanvas, newPos, Quaternion.identity);  
    }

    private void RemoveMarkers()
    {
        for(int i = 0; i < markersArray.Count; i++)
        {
            Destroy((GameObject)markersArray[i]);
        }
        markersArray.Clear();
    }
}
