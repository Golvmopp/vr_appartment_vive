﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class VRInputLeft : MonoBehaviour {

    public event Action OnTriggerPress;
    public event Action OnTriggerPressDown;
    public event Action OnTriggerPressUp;

    public event Action OnGripPress;
    public event Action OnGripPressDown;
    public event Action OnGripPressUp;

    public event Action OnTouchPadPress;
    public event Action OnTouchPadPressDown;
    public event Action OnTouchPadPressUp;

    private Valve.VR.EVRButtonId triggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
    private Valve.VR.EVRButtonId gripButton = Valve.VR.EVRButtonId.k_EButton_Grip;
    private Valve.VR.EVRButtonId touchPad = Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;

    private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
    private SteamVR_TrackedObject trackedObj;

    // Use this for initialization
    void Start()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (controller == null)
        {
            Debug.Log("Controller is not initialized");
            return;
        }

        CheckInput();
    }

    private void CheckInput()
    {
        if (controller.GetPress(triggerButton))
        {
            if (OnTriggerPress != null)
            {
                OnTriggerPress();
            }
        }
        if (controller.GetPressDown(triggerButton))
        {
            if (OnTriggerPressDown != null)
            {
                OnTriggerPressDown();
            }
        }
        if (controller.GetPressUp(triggerButton))
        {
            if (OnTriggerPressUp != null)
            {
                OnTriggerPressUp();
            }
        }

        if (controller.GetPress(gripButton))
        {
            if (OnGripPress != null)
            {
                OnGripPress();
            }
        }
        if (controller.GetPressDown(gripButton))
        {
            if (OnGripPressDown != null)
            {
                OnGripPressDown();
            }
        }
        if (controller.GetPressUp(gripButton))
        {
            if (OnGripPressUp != null)
            {
                OnGripPressUp();
            }
        }

        if (controller.GetPress(touchPad))
        {
            if(OnTouchPadPress != null)
            {
                OnTouchPadPress();
            }
        }
        if (controller.GetPressDown(touchPad))
        {
            if(OnTouchPadPressDown != null)
            {
                OnTouchPadPressDown();
            }
        }
        if (controller.GetPressUp(touchPad))
        {
            if(OnTouchPadPressDown != null)
            {
                OnTouchPadPressDown();
            }
        }
    }

    private void OnDestroy()
    {
        OnTriggerPress = null;
        OnTriggerPressDown = null;
        OnTriggerPressUp = null;

        OnGripPress = null;
        OnGripPressDown = null;
        OnGripPressUp = null;

        OnTouchPadPress = null;
        OnTouchPadPressDown = null;
        OnTouchPadPressUp = null;
    }
}
