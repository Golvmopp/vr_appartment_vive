﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MaterialChanger : MonoBehaviour {
    public VRInput m_VRInput;

    private Material[] objectMaterials;
    public Material newMaterial;

    public Renderer objectRenderer;
    public int meshMaterialNumber;

    public InteractableItem m_InteractableItem;

    public AudioClip paintSound;
    private AudioSource source;


    // Use this for initialization
    void Start () {
        source = GameObject.Find("VaggmaterialHallMenu").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update () {
	
	}

    private void OnEnable()
    {
        m_VRInput.OnTouchPadPressDown += TouchPadPressDown;
    }

    private void OnDisable()
    {
        m_VRInput.OnTouchPadPressDown -= TouchPadPressDown;
    }

    public void TouchPadPressDown(InteractableItem interactableItem)
    {
        if (interactableItem == m_InteractableItem)
        {
            objectMaterials = objectRenderer.materials;
            objectMaterials[meshMaterialNumber] = newMaterial;
            objectRenderer.materials = objectMaterials;

            source.PlayOneShot(paintSound, 1f);
        }
    }

}
