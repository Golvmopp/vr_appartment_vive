﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

    public VRInput m_VRInput;

    private float speed = 3f;

    public Transform cameraRig;
    public Camera m_camera;

    // Use this for initialization
    void Start () {
	
	}

    private void OnEnable()
    {
        m_VRInput.OnTriggerPress += TriggerPress;
    }

    private void OnDisable()
    {
        m_VRInput.OnTriggerPress -= TriggerPress;
    }
	
	// Update is called once per frame
	void Update () {
	    
	}

    private void TriggerPress()
    {
        cameraRig.position += new Vector3(m_camera.transform.forward.x * speed * Time.deltaTime, 0, m_camera.transform.forward.z * speed * Time.deltaTime);
    }
}
